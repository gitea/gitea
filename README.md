# About

A temporary repository to allow users report issues when gitea is migrating from
Github to gitea.com.

All these issues and comments could be added back to the new gitea repository with different 
issue number after gitea migrated.